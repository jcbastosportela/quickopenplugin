﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using NppPluginNET;
using System.Text.RegularExpressions;

namespace QuickOpenPlugin
{
    class Main
    {
        #region " Fields "
        internal const string PluginName = "QuickOpenPlugin";

        static Bitmap tbBmp = Properties.Resources.folder_open;
        static Bitmap tbBmp_tbTab = Properties.Resources.folder_open;
        //static Icon tbIcon = null;
        #endregion

        #region " StartUp/CleanUp "
        internal static void CommandMenuInit()
        {
 
            PluginBase.SetCommand(0, "Quick Open", QuickOpen, new ShortcutKey(false, true, false, Keys.O));
        }
        internal static void SetToolBarIcon()
        {
            toolbarIcons tbIcons = new toolbarIcons();
            tbIcons.hToolbarBmp = tbBmp.GetHbitmap();
            IntPtr pTbIcons = Marshal.AllocHGlobal(Marshal.SizeOf(tbIcons));
            Marshal.StructureToPtr(tbIcons, pTbIcons, false);
            Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_ADDTOOLBARICON, PluginBase._funcItems.Items[0]._cmdID, pTbIcons);
            Marshal.FreeHGlobal(pTbIcons);
        }
        internal static void PluginCleanUp()
        {
            //Win32.WritePrivateProfileString("SomeSection", "SomeKey", someSetting ? "1" : "0", iniFilePath);
        }
        #endregion

        #region " Menu functions "
        internal static void QuickOpen()
        {
            try
            {
                string textselected = "";

                int start = (int)Win32.SendMessage(PluginBase.GetCurrentScintilla(), SciMsg.SCI_GETSELECTIONSTART, 0, 0);
                int end = (int)Win32.SendMessage(PluginBase.GetCurrentScintilla(), SciMsg.SCI_GETSELECTIONEND, 0, 0);
                int textLen = end - start;
                StringBuilder sbsel = new StringBuilder(textLen + 1);
                Win32.SendMessage(PluginBase.GetCurrentScintilla(), SciMsg.SCI_GETSELTEXT, 0, sbsel);

                if (sbsel.ToString() != string.Empty)
                {
                    textselected = sbsel.ToString();
                }
                else
                {

                    StringBuilder sb = new StringBuilder(Win32.MAX_PATH);
                    Win32.SendMessage(PluginBase.GetCurrentScintilla(), SciMsg.SCI_GETCURLINE, 0, sb);

                    textselected = sb.ToString();
                }


                StringBuilder path = new StringBuilder(Win32.MAX_PATH);
                Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_GETCURRENTDIRECTORY, 0, path);
                string curpath = path.ToString();

                MatchCollection m1 = Regex.Matches(textselected, @"(((\.)*\./)*[0-9a-zA-Z_][0-9a-zA-Z/_-]*\.[0-9a-zA-Z_-]*)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
                foreach (Match m in m1)
                {
                    if (m.Success)
                    {
                        String file = m.Groups[1].ToString();
                        if (file.Substring(0, 2).Equals("./"))
                        {
                            file = textselected.Substring(2);
                        }

                        while (file.Substring(0, 3).Equals("../"))
                        {
                            curpath = Directory.GetParent(curpath).ToString();
                            file = textselected.Substring(3);
                        }

                        string fullpath = Path.Combine(curpath, file);

                        if (File.Exists(fullpath))
                            Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_DOOPEN, 0, fullpath);
                        else
                        {
                            while (!curpath.Equals(""))
                            {
                                curpath = Directory.GetParent(curpath).ToString();
                                if (File.Exists(Path.Combine(curpath, file)))
                                    Win32.SendMessage(PluginBase.nppData._nppHandle, NppMsg.NPPM_DOOPEN, 0, Path.Combine(curpath, file));
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
               // MessageBox.Show(e.Message);
            }
          
        }
        #endregion
    }
}